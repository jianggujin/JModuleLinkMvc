# 第一部分 简介

`JModuleLinkMvc`是`JModuleLink`的`MVC`实现，提供了用于在`Action`中处理请求与相应的方法，即使不使用`JModuleLink`，`JModuleLinkMvc`也可以帮助我们快速构建普通项目。

# 第二部分 开始使用

使用`JModuleLinkMvc`可以直接下载源代码编译或者下载已经编译的`jar`文件，如果您是使用`maven`来构建项目，也可以直接在`pom.xml`中添加`JModuleLinkMvc`的坐标：

[![Maven central](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JModuleLinkMvc/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JModuleLinkMvc)

```xml
<!-- http://mvnrepository.com/artifact/com.jianggujin/JModuleLinkMvc -->
<dependency>
    <groupId>com.jianggujin</groupId>
    <artifactId>JModuleLinkMvc</artifactId>
    <version>最新版本</version>
</dependency>
```

最新的版本可以从[Maven仓库](http://mvnrepository.com/artifact/com.jianggujin/JModuleLinkMvc)或者[码云](https://gitee.com/jianggujin)获取。

## 2.1 JActionContext

`JActionContext为`模块执行动作的上下文，提供丰富的API处理客户端请求以及常用的操作，即使您不使用`JModuleLinkMvc`的多模块功能，也可以借助`JActionContext`完成基本的请求处理，比如参数转`JAVA Bean`等。我们可以通过如下方式获得一个`JActionContext`对象

```java
JActionContext context = new JActionContextImpl(request, response);
```

> 需要注意的是`JAcgionContext`封装了常用的方法，如果该类中提供的方法不满足实际需要，可以尝试使用`JWebUtils `类，该类中存在一些扩展方法。

### 2.1.1 参数处理

在`JActionContext`中有很多名字以`getPara`开头的方法，该方法用于获取客户端的请求参数，并且可以将请求参数转化为`Integer`、`Long`、`Boolean`等常用的数据类型。

```java
/**
 * 获得请求参数
 * 
 * @param name
 * @return
 */
String getPara(String name);

/**
 * 获得请求参数
 * 
 * @param name
 * @param defaultValue
 * @return
 */
String getPara(String name, String defaultValue);

/**
 * 获得请求参数
 * 
 * @param name
 * @return
 */
String[] getParaValues(String name);

/**
 * 获得整型请求参数
 * 
 * @param name
 * @return
 */
Integer getParaToInt(String name);

/**
 * 获得整型请求参数
 * 
 * @param name
 * @param defaultValue
 * @return
 */
Integer getParaToInt(String name, Integer defaultValue);

/**
 * 获得长整型请求参数
 * 
 * @param name
 * @return
 */
Long getParaToLong(String name);

/**
 * 获得长整型请求参数
 * 
 * @param name
 * @param defaultValue
 * @return
 */
Long getParaToLong(String name, Long defaultValue);

/**
 * 获得布尔型请求参数
 * 
 * @param name
 * @return
 */
Boolean getParaToBoolean(String name);

/**
 * 获得布尔型请求参数
 * 
 * @param name
 * @param defaultValue
 * @return
 */
Boolean getParaToBoolean(String name, Boolean defaultValue);

/**
 * 获得日期型请求参数
 * 
 * @param name
 * @return
 */
Date getParaToDate(String name);

/**
 * 获得日期型请求参数
 * 
 * @param name
 * @param defaultValue
 * @return
 */
Date getParaToDate(String name, Date defaultValue);

/**
 * 将请求参数转换为指定的JavaBean
 * 
 * @param clazz
 * @return
 */
<T> T getParaToBean(Class<T> clazz);

/**
 * 将请求参数转换为指定的JavaBean
 * 
 * @param clazz
 * @param skipConvertError
 * @return
 */
<T> T getParaToBean(Class<T> clazz, boolean skipConvertError);

/**
 * 获得指定类型请求参数
 * 
 * @param name
 * @param clazz
 * @return
 */
<T> T getPara(String name, Class<T> clazz);

/**
 * 获得指定类型请求参数
 * 
 * @param name
 * @param clazz
 * @param defaultValue
 * @return
 */
<T> T getPara(String name, Class<T> clazz, T defaultValue);

/**
 * 获得所有请求参数
 * 
 * @return
 */
Map<String, ?> getParaMap();

/**
 * 获得请求参数名称
 * 
 * @return
 */
Enumeration<String> getParaNames();
```

如果现有的数据转换器不满足实际使用需求，可以自定义转换器覆盖默认的转换器，或者对转换器进行扩展。编写转换器很容易，我们只需要实现`JConverter `接口，然后调用`JTypeConverter `类的`register `方法即可。默认的转换器如下：

```java
register(Integer.class, new JIntegerConverter());
register(int.class, new JIntegerConverter());
register(Long.class, new JLongConverter());
register(long.class, new JLongConverter());
register(Double.class, new JDoubleConverter());
register(double.class, new JDoubleConverter());
register(Float.class, new JFloatConverter());
register(float.class, new JFloatConverter());
register(Boolean.class, new JBooleanConverter());
register(boolean.class, new JBooleanConverter());
register(java.util.Date.class, new JDateConverter());
register(java.sql.Date.class, new JSqlDateConverter());
register(java.sql.Time.class, new JTimeConverter());
register(java.sql.Timestamp.class, new JTimestampConverter());
register(java.math.BigDecimal.class, new JBigDecimalConverter());
register(java.math.BigInteger.class, new JBigIntegerConverter());
register(byte[].class, new JByteArrayConverter());
register(Short.class, new JShortConverter());
register(short.class, new JShortConverter());
register(Byte.class, new JByteConverter());
register(byte.class, new JByteConverter());
```

有些特殊的业务场景可能需要修改请求参数，我们可以使用`setParaMap `方法，但是不建议这样做，避免因使用不当导致参数错误。

除了获取客户端的请求参数，我们可能还需要将一些请求参数回显回页面，使用`keepPara `可以很容易的做到。

```java
/**
 * 保持请求参数，会将请求参数回设变成{@link HttpServletRequest}属性
 */
JActionContext keepPara();

/**
 * 保持指定的请求参数，会将请求参数回设变成{@link HttpServletRequest}属性
 */
JActionContext keepPara(String... names);
```

上面介绍的都是获取普通的请求参数，有时候我们可能会在请求路径中添加特殊的参数用于处理，比如`Restful API`，针对这种情况，`JActionContext`也提供了获取指定位置的路径参数的方法，具体方法如下：

```java
/**
 * 获得所有路径参数
 */
String[] getPathParas();

/**
 * 获得路径参数个数
 */
int getPathCount();

/**
 * 获得路径参数
 */
String getPathPara(int pos);

/**
 * 获得整型路径参数
 */
Integer getPathParaToInt(int pos);

/**
 * 获得长整型路径参数
 */
Long getPathParaToLong(int pos);

/**
 * 获得布尔型路径参数
 */
Boolean getPathParaToBoolean(int pos);

/**
 * 获得指定类型的路径参数
 */
<T> T getPathPara(int pos, Class<T> clazz);
```

> 此处获得的路径参数是去除上下文路径后的结果

### 2.1.2 Cookie

除了请求参数的处理，`Cookie`的处理也是相对高频的操作，在`JActionContext`中，同样提供了针对`Cookie`的操作方法。

```java
/**
 * 获得Cookie
 */
String getCookie(String name, String defaultValue);

/**
 * 获得Cookie
 */
String getCookie(String name);

/**
 * 设置Cookie
 */
JActionContext setCookie(Cookie cookie);

/**
 * 移除Cookie
 */
JActionContext removeCookie(String name);
```

> JAVA EE5中`Cookie`没有处理`isHttpOnly`，如果您需要设置该属性，可以使用`JWebUtils.doSetCookie(HttpServletResponse, Cookie, Boolean)`方法，直接添加`Set-Cookie `响应头 

### 2.1.3 属性处理

`JActionContext`对`request`、`session`和`application`三个级别的属性操作都做了封装，可以直接使用。

```java
/**
 * 设置{@link HttpServletRequest}属性
 * 
 * @param name
 * @param value
 * @return
 */
JActionContext setAttr(String name, Object value);

/**
 * 获得{@link HttpServletRequest}属性
 * 
 * @param name
 * @return
 */
Object getAttr(String name);

/**
 * 设置{@link HttpSession}属性
 * 
 * @param name
 * @param value
 * @return
 */
JActionContext setSessionAttr(String name, Object value);

/**
 * 获得{@link HttpSession}属性
 * 
 * @param name
 * @return
 */
Object getSessionAttr(String name);

/**
 * 设置{@link ServletContext}属性
 * 
 * @param name
 * @param value
 * @return
 */
JActionContext setContextAttr(String name, Object value);

/**
 * 获得{@link ServletContext}属性
 * 
 * @param name
 * @return
 */
Object getContextAttr(String name);
```

### 2.1.4 请求体处理

如果客户端提交的不是传统的参数，而是`JSON`、`XML`等格式的数据，那么我们就需要对请求数据进行特殊处理，`JActionContext`提供了常用的辅助方法可以将请求的输入流转换所需要的数据。

```java
/**
 * 获得请求数据
 * 
 * @return
 * @throws IOException
 */
byte[] getRequestData() throws IOException;

/**
 * 获得请求体，将请求输入流转换为字符串
 * 
 * @return
 * @throws IOException
 */
String getRequestBody() throws IOException;

/**
 * 获得请求体，将请求输入流转换为字符串
 * 
 * @param charset
 * @return
 * @throws IOException
 */
String getRequestBody(String charset) throws IOException;

/**
 * 获得JSON请求数据
 * 
 * @param clazz
 * @return
 * @throws IOException
 */
<T> T getRequestJson(Class<T> clazz) throws IOException;

/**
 * 获得JSON请求数据
 * 
 * @param clazz
 * @param charset
 * @return
 */
<T> T getRequestJson(Class<T> clazz, String charset);

/**
 * 获得XML请求数据
 * 
 * @param clazz
 * @return
 * @throws IOException
 */
<T> T getRequestXml(Class<T> clazz) throws IOException;

/**
 * 获得XML请求数据
 * 
 * @param clazz
 * @param charset
 * @return
 */
<T> T getRequestXml(Class<T> clazz, String charset);
```

`JSON`与`XML`的数据解析需要三方包支持，如果您需要复杂的解析操作，可以参见`JRequestResolverManager `的使用。

### 2.1.5 上传文件

上传文件是请求处理中必不可少的一环，`JActionContext`也提供了相应API进行上传文件的处理。

```java
/**
 * 获得所有上传文件
 */
JMultiValueMap<String, JMultipartFile> getMultipartFiles();

/**
 * 获得上传文件
 */
JMultipartFile getFile(String name);

/**
 * 获得上传文件
 */
List<JMultipartFile> getFiles(String name);

/**
 * 清除上传文件
 */
void cleanupMultipart();

/**
 * 解析上传文件
 */
void parseMultipart(JMultipartResolverConfig config);
```

如果您的Web容器选择的是`Tomcat`，则不需要添加任何依赖，默认兼容`Tomcat`，否则需要添加`commons-fileupload`与`commons-io`包。`JModuleLinkMvc`默认会先加载`Tomcat`，然后加载`commons`包，如果二者都不满足，则无法使用上传文件功能。

如果您有自己喜欢的解析方式，也可以指定自己的上传文件解析的类实现，该类需要实现`JMultipartResolver `接口，然后调用`JMultipartResolverFactory `类的`useMultipartResolver `方法即可。

`commons-fileupload`的`maven`坐标如下：

```xml
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>1.3.1</version>
</dependency>
```

### 2.1.6 响应渲染

以上部分都是处理客户端的请求，有请求自然会有响应，`JActionContext`默认集成了一些常用的渲染。

```java
 /**
 * 文本响应
 * 
 * @param txt
 * @throws ServletException
 * @throws IOException
 */
void renderText(String txt) throws IOException, ServletException;

/**
 * 请求转发
 * 
 * @param path
 * @throws ServletException
 * @throws IOException
 */
void renderDispatcher(String path) throws IOException, ServletException;

/**
 * 重定向
 * 
 * @param path
 * @throws ServletException
 * @throws IOException
 */
void renderRedirect(String path) throws IOException, ServletException;

/**
 * 异常响应
 * 
 * @param code
 * @throws IOException
 * @throws ServletException
 */
void renderError(int code) throws IOException, ServletException;

/**
 * JSON响应
 * 
 * @param json
 * @throws IOException
 * @throws ServletException
 */
void renderJson(Object json) throws IOException, ServletException;

/**
 * XML响应
 * 
 * @param xml
 * @throws IOException
 * @throws ServletException
 */
void renderXml(Object xml) throws IOException, ServletException;
```

`JSON`与`XML`的数据解析需要三方包支持，如果上述渲染您的需求，您可以使用`JRenderManager `类来进行不同业务要求的渲染。

## 2.2 JRequestResolverManager 

该类为请求解析器的管理类，使用该类可以将`JSON`或`XML`格式的请求解析为`Java Bean`，对于`JSON`格式的数据解析，`JModuleLinkMvc`默认集成了`jackson`和`fastjson`，加载顺序为：`jackson`>`fastjson`。`XML`格式的数据解析，`JModuleLinkMvc`默认集成了`jackson`、`XStream`和`JAXB`，加载顺序为：`jackson`>`XStream`>`JAXB`。

如果默认集成的解析器不满足实际需求， 可以编写自己的实现类，该类需要实现`JRequestResolver `接口，然后调用`JRequestResolverManager`类的`useJsonResolver `方法或`useXmlResolver `方法。

在该类中提供了如下四个方法，可用于将客户端请求数据转换为Java Bean。

```java
public static <T> T getBeanFromBodyWithJson(Class<T> clazz, JActionContext context)
      throws IOException;

public static <T> T getBeanFromBodyWithJson(Class<T> clazz, JActionContext context, 
      String charset);

public static <T> T getBeanFromBodyWithXml(Class<T> clazz, JActionContext context)
      throws IOException;

public static <T> T getBeanFromBodyWithXml(Class<T> clazz, JActionContext context,
      String charset);
```

相关三方包的`maven`坐标如下：

```xml
<dependency>
    <groupId>com.thoughtworks.xstream</groupId>
    <artifactId>xstream</artifactId>
    <version>1.4.9</version>
</dependency>
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-annotations</artifactId>
    <version>2.8.0</version>
</dependency>
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-core</artifactId>
    <version>2.8.10</version>
</dependency>
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.8.10</version>
</dependency>
<dependency>
    <groupId>com.fasterxml.jackson.dataformat</groupId>
    <artifactId>jackson-dataformat-xml</artifactId>
    <version>2.8.10</version>
</dependency>
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.9</version>
</dependency>
```

## 2.3 JRenderManager 

该类为响应渲染器管理器，`JModuleLinkMvc`内置的渲染器都可以在该类中找到，同时该类支持注册自定义渲染器。该类默认集成了`验证码`、`异常`、`文件`、`JSON`、`二维码`、`重定向`、`请求转发`、`文本`和`XML`的渲染，这些渲染器并不一定全部可以正常工作，部分需要依赖三方包。

对于`JSON`和`XML`格式数据的响应与`JRequestResolverManager`类似，依赖的三方包可参考`JRequestResolverManager`。

二维码的响应渲染器集成的是`zxing `包，`maven`坐标如下：

```xml
<dependency>
    <groupId>com.google.zxing</groupId>
    <artifactId>javase</artifactId>
    <version>3.2.1</version>
</dependency>
```
## 2.4 JGenericHttpAction和JHttpAction

为了更好的利用`JModuleLink`处理Web请求，`JModuleLinkMvc`中默认提供了两个`JAction`的实现，如果我们的操作是与请求方法无关的，那么我们在编写`JAction`的实现时可以直接继承`JGenericHttpAction`，反之则使用`JHttpAction`，这两者的关系有点类似`GenericServlet`与`HttpServlet`。