/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.resolver.impl;

import java.io.InputStreamReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.resolver.JRequestResolveException;
import com.jianggujin.modulelink.mvc.resolver.JRequestResolver;
import com.jianggujin.modulelink.mvc.util.JIOUtils;
import com.jianggujin.modulelink.mvc.util.JWebUtils;
import com.jianggujin.modulelink.util.JAssert;

public class JJAXBXmlRequestResolver implements JRequestResolver {
   private final static String FRATURE_EXTERNAL_GENERAL_ENTITIES = "http://xml.org/sax/features/external-general-entities";
   private final static String FRATURE_EXTERNAL_PARAMETER_ENTITIES = "http://xml.org/sax/features/external-parameter-entities";
   private final static String FRATURE_LOAD_EXTERNAL_DTD = "http://apache.org/xml/features/nonvalidating/load-external-dtd";

   @Override
   public <T> T resolve(Class<T> clazz, JActionContext context, String charset) {
      JAssert.checkNotNull(context, "context must not be null");
      try {
         charset = charset == null ? JWebUtils.getRequestEncoding(context.getRequest()) : charset;
         InputStreamReader reader = new InputStreamReader(context.getRequest().getInputStream(), charset);
         SAXParserFactory saxParserFactory = newSAXParserFactory();
         Source xmlSource = new SAXSource(saxParserFactory.newSAXParser().getXMLReader(), new InputSource(reader));
         T t = (T) unmarshal(clazz, xmlSource);
         JIOUtils.safeClose(reader);
         return t;
      } catch (Exception e) {
         throw new JRequestResolveException(e);
      }
   }

   @SuppressWarnings("unchecked")
   public static <T> T unmarshal(Class<T> classesToBeBound, Source source) throws JAXBException {
      JAXBContext jc = JAXBContext.newInstance(classesToBeBound);
      Unmarshaller um = jc.createUnmarshaller();
      return (T) um.unmarshal(source);
   }

   public static SAXParserFactory newSAXParserFactory() throws ParserConfigurationException, SAXException {
      SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
      saxParserFactory.setFeature(FRATURE_EXTERNAL_GENERAL_ENTITIES, false);
      saxParserFactory.setFeature(FRATURE_EXTERNAL_PARAMETER_ENTITIES, false);
      saxParserFactory.setFeature(FRATURE_LOAD_EXTERNAL_DTD, false);
      return saxParserFactory;
   }
}
