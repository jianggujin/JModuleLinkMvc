/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.resolver;

/**
 * 请求解析异常
 * 
 * @author jianggujin
 *
 */
public class JRequestResolveException extends RuntimeException {

   private static final long serialVersionUID = 1L;

   public JRequestResolveException(Throwable cause) {
      super(cause);
   }

   public JRequestResolveException(String message) {
      super(message);
   }

   public JRequestResolveException(String message, Throwable cause) {
      super(message, cause);
   }

}
