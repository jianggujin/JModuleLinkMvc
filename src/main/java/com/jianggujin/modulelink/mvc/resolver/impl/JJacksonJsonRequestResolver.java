/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.resolver.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.resolver.JRequestResolveException;
import com.jianggujin.modulelink.mvc.resolver.JRequestResolver;
import com.jianggujin.modulelink.util.JAssert;

public class JJacksonJsonRequestResolver implements JRequestResolver {
   private static ObjectMapper objectMapper = new ObjectMapper();

   @Override
   public <T> T resolve(Class<T> clazz, JActionContext context, String charset) {
      JAssert.checkNotNull(context, "context must not be null");
      try {
         return objectMapper.readValue(context.getRequestBody(charset), clazz);
      } catch (Exception e) {
         throw new JRequestResolveException(e);
      }
   }

   public static void setXmlMapper(ObjectMapper objectMapper) {
      JJacksonJsonRequestResolver.objectMapper = objectMapper;
   }
}
