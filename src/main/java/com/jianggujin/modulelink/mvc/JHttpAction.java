/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HTTP请求处理
 * 
 * @author jianggujin
 *
 */
public abstract class JHttpAction extends JGenericHttpAction {
   private static final String METHOD_DELETE = "DELETE";
   private static final String METHOD_HEAD = "HEAD";
   private static final String METHOD_GET = "GET";
   private static final String METHOD_OPTIONS = "OPTIONS";
   private static final String METHOD_POST = "POST";
   private static final String METHOD_PUT = "PUT";
   private static final String METHOD_TRACE = "TRACE";

   private static final String HEADER_IFMODSINCE = "If-Modified-Since";
   private static final String HEADER_LASTMOD = "Last-Modified";

   protected Object service(JActionContext context) throws IOException, ServletException {
      HttpServletRequest req = context.getRequest();
      HttpServletResponse resp = context.getResponse();
      String method = req.getMethod().toUpperCase();

      if (method.equals(METHOD_GET)) {
         long lastModified = getLastModified(req);
         if (lastModified == -1) {
            doGet(context);
         } else {
            long ifModifiedSince = req.getDateHeader(HEADER_IFMODSINCE);
            if (ifModifiedSince < (lastModified / 1000 * 1000)) {
               maybeSetLastModified(resp, lastModified);
               doGet(context);
            } else {
               resp.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            }
         }
      } else if (method.equals(METHOD_HEAD)) {
         long lastModified = getLastModified(req);
         maybeSetLastModified(resp, lastModified);
         doHead(context);
      } else if (method.equals(METHOD_POST)) {
         doPost(context);
      } else if (method.equals(METHOD_PUT)) {
         doPut(context);
      } else if (method.equals(METHOD_DELETE)) {
         doDelete(context);
      } else if (method.equals(METHOD_OPTIONS)) {
         doOptions(context);
      } else if (method.equals(METHOD_TRACE)) {
         doTrace(context);
      } else {
         resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "http method:" + method + " not implemented");
      }
      return null;
   }

   protected void doGet(JActionContext context) throws IOException, ServletException {
      throw new UnsupportedOperationException("http method:GET not implemented");
   }

   protected void doHead(JActionContext context) {
      throw new UnsupportedOperationException("http method:HEAD not implemented");
   }

   protected void doPost(JActionContext context) throws IOException, ServletException {
      throw new UnsupportedOperationException("http method:POST not implemented");

   }

   protected void doPut(JActionContext context) throws IOException, ServletException {
      throw new UnsupportedOperationException("http method:PUT not implemented");

   }

   protected void doDelete(JActionContext context) throws IOException, ServletException {
      throw new UnsupportedOperationException("http method:DELETE not implemented");

   }

   protected void doOptions(JActionContext context) throws IOException, ServletException {
      throw new UnsupportedOperationException("http method:OPTIONS not implemented");

   }

   protected void doTrace(JActionContext context) throws IOException, ServletException {
      throw new UnsupportedOperationException("http method:TRACE not implemented");
   }

   protected long getLastModified(HttpServletRequest req) {
      return -1;
   }

   private void maybeSetLastModified(HttpServletResponse resp, long lastModified) {
      if (resp.containsHeader(HEADER_LASTMOD))
         return;
      if (lastModified >= 0)
         resp.setDateHeader(HEADER_LASTMOD, lastModified);
   }

}
