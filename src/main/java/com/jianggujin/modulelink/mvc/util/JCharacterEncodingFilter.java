/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class JCharacterEncodingFilter implements Filter {
   private String encoding;

   private boolean forceEncoding = false;

   public void setEncoding(String encoding) {
      this.encoding = encoding;
   }

   public void setForceEncoding(boolean forceEncoding) {
      this.forceEncoding = forceEncoding;
   }

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
         throws ServletException, IOException {
      if (this.encoding != null && (this.forceEncoding || request.getCharacterEncoding() == null)) {
         request.setCharacterEncoding(this.encoding);
         if (this.forceEncoding) {
            response.setCharacterEncoding(this.encoding);
         }
      }
      filterChain.doFilter(request, response);
   }

   @Override
   public void init(FilterConfig config) throws ServletException {
      this.encoding = config.getInitParameter("encoding");
      this.forceEncoding = "true".equals(config.getInitParameter("forceEncoding"));
   }

   @Override
   public void destroy() {

   }
}
