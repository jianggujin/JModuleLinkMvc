/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.util.captcha;

import java.awt.Color;
import java.awt.Font;
import java.io.OutputStream;

/**
 * 验证码抽象类
 * 
 * @author jianggujin
 *
 */
public abstract class JCaptcha extends JRandoms {
   protected Font font = new Font("Verdana", Font.ITALIC | Font.BOLD, 28); // 字体
   protected int len = 5; // 验证码随机字符长度
   protected int width = 150; // 验证码显示跨度
   protected int height = 40; // 验证码显示高度
   private String chars = null; // 随机字符串

   /**
    * 生成随机字符数组
    * 
    * @return 字符数组
    */
   protected char[] alphas() {
      char[] cs = new char[len];
      for (int i = 0; i < len; i++) {
         cs[i] = alpha();
      }
      chars = new String(cs);
      return cs;
   }

   public Font getFont() {
      return font;
   }

   public void setFont(Font font) {
      this.font = font;
   }

   public int getLen() {
      return len;
   }

   public void setLen(int len) {
      this.len = len;
   }

   public int getWidth() {
      return width;
   }

   public void setWidth(int width) {
      this.width = width;
   }

   public int getHeight() {
      return height;
   }

   public void setHeight(int height) {
      this.height = height;
   }

   /**
    * 给定范围获得随机颜色
    * 
    * @return Color 随机颜色
    */
   protected Color color(int fc, int bc) {
      if (fc > 255)
         fc = 255;
      if (bc > 255)
         bc = 255;
      int r = fc + num(bc - fc);
      int g = fc + num(bc - fc);
      int b = fc + num(bc - fc);
      return new Color(r, g, b);
   }

   /**
    * 验证码输出,抽象方法，由子类实现
    * 
    * @param os
    *           输出流
    */
   public abstract void out(OutputStream os);

   /**
    * 获取随机字符串
    * 
    * @return string
    */
   public String text() {
      return chars;
   }
}
