/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.upload;

import java.io.File;

/**
 * 解析配置
 * 
 * @author jianggujin
 *
 */
public class JMultipartResolverConfig {
   public static final int DEFAULT_SIZE_THRESHOLD = 10240;
   private String encoding;
   private long sizeMax = -1;
   private long fileSizeMax = -1;
   private int sizeThreshold = DEFAULT_SIZE_THRESHOLD;
   private File repository;
   private JProgressListener progressListener;

   public String getEncoding() {
      return encoding;
   }

   public void setEncoding(String encoding) {
      this.encoding = encoding;
   }

   public long getSizeMax() {
      return sizeMax;
   }

   public void setSizeMax(long sizeMax) {
      this.sizeMax = sizeMax;
   }

   public long getFileSizeMax() {
      return fileSizeMax;
   }

   public void setFileSizeMax(long fileSizeMax) {
      this.fileSizeMax = fileSizeMax;
   }

   public int getSizeThreshold() {
      return sizeThreshold;
   }

   public void setSizeThreshold(int sizeThreshold) {
      this.sizeThreshold = sizeThreshold;
   }

   public File getRepository() {
      return repository;
   }

   public void setRepository(File repository) {
      this.repository = repository;
   }

   public JProgressListener getProgressListener() {
      return progressListener;
   }

   public void setProgressListener(JProgressListener progressListener) {
      this.progressListener = progressListener;
   }

}
