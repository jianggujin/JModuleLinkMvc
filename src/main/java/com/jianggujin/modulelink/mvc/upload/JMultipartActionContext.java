/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.upload;

import java.util.List;

import com.jianggujin.modulelink.util.JMultiValueMap;

/**
 * 支持上传文件解析的上下文
 * 
 * @author jianggujin
 *
 */
public interface JMultipartActionContext {
   /**
    * 获得所有上传文件
    * 
    * @return
    */
   JMultiValueMap<String, JMultipartFile> getMultipartFiles();

   /**
    * 获得上传文件
    * 
    * @param name
    * @return
    */
   JMultipartFile getFile(String name);

   /**
    * 获得上传文件
    * 
    * @param name
    * @return
    */
   List<JMultipartFile> getFiles(String name);

   /**
    * 清除上传文件
    */
   void cleanupMultipart();

   /**
    * 解析上传文件
    * 
    * @param config
    */
   void parseMultipart(JMultipartResolverConfig config);
}
