/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.upload;

import java.util.Map;

import com.jianggujin.modulelink.util.JMultiValueMap;

/**
 * 解析结果
 * 
 * @author jianggujin
 *
 */
public class JMultipartParsingResult {

   private final JMultiValueMap<String, JMultipartFile> multipartFiles;

   private final Map<String, String[]> multipartParameters;

   public JMultipartParsingResult(JMultiValueMap<String, JMultipartFile> mpFiles, Map<String, String[]> mpParams) {
      this.multipartFiles = mpFiles;
      this.multipartParameters = mpParams;
   }

   public JMultiValueMap<String, JMultipartFile> getMultipartFiles() {
      return this.multipartFiles;
   }

   public Map<String, String[]> getMultipartParameters() {
      return this.multipartParameters;
   }

}
