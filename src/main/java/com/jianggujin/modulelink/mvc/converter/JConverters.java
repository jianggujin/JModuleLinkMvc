/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.converter;

import java.text.ParseException;

import com.jianggujin.modulelink.mvc.util.JDateUtils;

/**
 * 针对 Integer、Long、Date 等类型实现 JConverter 接口
 * 
 * @author jianggujin
 *
 */
public class JConverters {

   private JConverters() {
   }

   public static class JIntegerConverter implements JConverter<Integer> {

      @Override
      public Integer convert(String s) throws JConvertException {
         return Integer.parseInt(s);
      }

   }

   public static class JShortConverter implements JConverter<Short> {
      @Override
      public Short convert(String s) throws JConvertException {
         return Short.parseShort(s);
      }
   }

   public static class JByteConverter implements JConverter<Byte> {
      @Override
      public Byte convert(String s) throws JConvertException {
         return Byte.parseByte(s);
      }
   }

   public static class JLongConverter implements JConverter<Long> {
      @Override
      public Long convert(String s) throws JConvertException {
         return Long.parseLong(s);
      }
   }

   public static class JFloatConverter implements JConverter<Float> {
      @Override
      public Float convert(String s) throws JConvertException {
         return Float.parseFloat(s);
      }
   }

   public static class JDoubleConverter implements JConverter<Double> {
      @Override
      public Double convert(String s) throws JConvertException {
         return Double.parseDouble(s);
      }
   }

   public static class JByteArrayConverter implements JConverter<byte[]> {
      @Override
      public byte[] convert(String s) throws JConvertException {
         return s.getBytes();
      }
   }

   public static class JBigIntegerConverter implements JConverter<java.math.BigInteger> {
      @Override
      public java.math.BigInteger convert(String s) throws JConvertException {
         return new java.math.BigInteger(s);
      }
   }

   public static class JBigDecimalConverter implements JConverter<java.math.BigDecimal> {
      @Override
      public java.math.BigDecimal convert(String s) throws JConvertException {
         return new java.math.BigDecimal(s);
      }
   }

   public static class JBooleanConverter implements JConverter<Boolean> {
      @Override
      public Boolean convert(String s) {
         String value = s.toLowerCase();
         if ("true".equals(value) || "1".equals(value) || "yes".equals(value) || "on".equals(value)) {
            return Boolean.TRUE;
         } else if ("false".equals(value) || "0".equals(value) || "no".equals(value) || "off".equals(value)) {
            return Boolean.FALSE;
         } else {
            throw new JConvertException("Can not parse to boolean type of value: " + s);
         }
      }
   }

   private static final String[] FORMATS = { "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd",
         "yyyyMMdd", "HH:mm:ss", "HHmmss" };

   public static class JDateConverter implements JConverter<java.util.Date> {

      @Override
      public java.util.Date convert(String s) throws JConvertException {
         try {
            return JDateUtils.parseDateStrictly(s, FORMATS);
         } catch (ParseException e) {
         }
         throw new JConvertException("Can not parse to java.util.Date type of value:" + s);
      }
   }

   public static class JSqlDateConverter implements JConverter<java.sql.Date> {
      @Override
      public java.sql.Date convert(String s) throws JConvertException {
         try {
            return new java.sql.Date(JDateUtils.parseDateStrictly(s, FORMATS).getTime());
         } catch (ParseException e) {
            throw new JConvertException("Can not parse to java.sql.Date type of value:" + s, e);
         }
      }
   }

   public static class JTimeConverter implements JConverter<java.sql.Time> {
      @Override
      public java.sql.Time convert(String s) throws JConvertException {
         try {
            return new java.sql.Time(JDateUtils.parseDateStrictly(s, FORMATS).getTime());
         } catch (ParseException e) {
            throw new JConvertException("Can not parse to java.sql.Timestamp type of value:" + s, e);
         }
      }
   }

   public static class JTimestampConverter implements JConverter<java.sql.Timestamp> {
      @Override
      public java.sql.Timestamp convert(String s) throws JConvertException {
         try {
            return new java.sql.Timestamp(JDateUtils.parseDateStrictly(s, FORMATS).getTime());
         } catch (ParseException e) {
            throw new JConvertException("Can not parse to java.sql.Timestamp type of value:" + s, e);
         }
      }
   }
}
