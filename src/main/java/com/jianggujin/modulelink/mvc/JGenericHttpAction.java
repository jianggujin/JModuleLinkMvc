/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc;

import java.io.IOException;

import javax.servlet.ServletException;

import com.jianggujin.modulelink.JModuleContext;
import com.jianggujin.modulelink.exception.JModuleLinkException;
import com.jianggujin.modulelink.support.JAbstractAction;
import com.jianggujin.modulelink.util.JExceptionUtils;

/**
 * HTTP请求处理
 * 
 * @author jianggujin
 *
 */
public abstract class JGenericHttpAction extends JAbstractAction {
    @Override
    public Object execute(JModuleContext context) throws JModuleLinkException {
        JActionContext actionContext = new JActionContextImpl(context);
        Object result = null;
        try {
            result = service(actionContext);
        } catch (Throwable e) {
            JExceptionUtils.handleThrowable(e);
        }
        actionContext.cleanupMultipart();
        return result;
    }

    /**
     * 请求处理
     * 
     * @param context
     * @return
     * @throws IOException
     * @throws ServletException
     */
    protected abstract Object service(JActionContext context) throws IOException, ServletException;
}
