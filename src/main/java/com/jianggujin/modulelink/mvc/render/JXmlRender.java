/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render;

import java.io.IOException;
import java.io.OutputStream;

import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.util.JWebUtils;
import com.jianggujin.modulelink.util.JAssert;

/**
 * XML渲染
 * 
 * @author jianggujin
 *
 */
public class JXmlRender implements JRender<Object> {
   private static JBeanXmlRender beanXmlConverter = null;

   static {
      tryImplementation(new Runnable() {
         @Override
         public void run() {
            useJacksonBeanXmlConverter();
         }
      });
      tryImplementation(new Runnable() {
         @Override
         public void run() {
            useXStreamBeanXmlConverter();
         }
      });
      tryImplementation(new Runnable() {
         @Override
         public void run() {
            useJAXBBeanXmlConverter();
         }
      });
   }

   @Override
   public void render(JActionContext context, Object param) throws IOException {
      if (JXmlRender.beanXmlConverter == null) {
         throw new IllegalStateException("nou set bean xml convert.");
      }
      JAssert.checkNotNull(context, "context must not be null");
      JAssert.checkNotNull(param, "param must not be null");

      String encoding = JWebUtils.getResponseEncoding(context.getResponse());
      context.getResponse().setContentType("text/xml; charset=" + encoding);
   }

   /**
    * Bean - Xml 渲染
    * 
    * @author jianggujin
    *
    */
   public static interface JBeanXmlRender {
      void render(Object bean, OutputStream out, String encoding);
   }

   /**
    * 设置使用的转换器
    * 
    * @param beanXmlConverter
    */
   public static void useBeanXmlConverter(JBeanXmlRender beanXmlConverter) {
      JXmlRender.beanXmlConverter = beanXmlConverter;
   }

   public static synchronized void useJacksonBeanXmlConverter() {
      JXmlRender.beanXmlConverter = new com.jianggujin.modulelink.mvc.render.impl.JJacksonBeanXmlRender();
   }

   public static synchronized void useJAXBBeanXmlConverter() {
      JXmlRender.beanXmlConverter = new com.jianggujin.modulelink.mvc.render.impl.JJAXBBeanXmlRender();
   }

   public static synchronized void useXStreamBeanXmlConverter() {
      JXmlRender.beanXmlConverter = new com.jianggujin.modulelink.mvc.render.impl.JXStreamBeanXmlRender();
   }

   private static void tryImplementation(Runnable runnable) {
      if (beanXmlConverter == null) {
         try {
            runnable.run();
         } catch (Throwable t) {
            // ignore
         }
      }
   }
}
