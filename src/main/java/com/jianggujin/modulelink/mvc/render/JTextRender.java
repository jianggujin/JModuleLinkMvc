/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.util.JWebUtils;
import com.jianggujin.modulelink.util.JAssert;

/**
 * 文本渲染
 * 
 * @author jianggujin
 *
 */
public class JTextRender implements JRender<String> {

   @Override
   public void render(JActionContext context, String param) throws IOException, ServletException {
      JAssert.checkNotNull(context, "context must not be null");
      JAssert.checkNotNull(param, "param must not be null");

      String encoding = JWebUtils.getResponseEncoding(context.getResponse());
      context.getResponse().setContentType("text/plain; charset=" + encoding);
      PrintWriter writer = context.getResponse().getWriter();
      writer.write(param);
      writer.flush();
   }

}
