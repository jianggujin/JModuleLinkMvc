/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render;

import java.io.IOException;
import java.io.OutputStream;

import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.util.JWebUtils;
import com.jianggujin.modulelink.util.JAssert;

/**
 * JSON渲染
 * 
 * @author jianggujin
 *
 */
public class JJsonRender implements JRender<Object> {
   private static JBeanJsonRender beanJsonConverter = null;

   static {
      tryImplementation(new Runnable() {
         @Override
         public void run() {
            useJacksonBeanJsonConverter();
         }
      });
      tryImplementation(new Runnable() {
         @Override
         public void run() {
            useFastjsonBeanJsonConverter();
         }
      });
   }

   @Override
   public void render(JActionContext context, Object param) throws IOException {
      if (JJsonRender.beanJsonConverter == null) {
         throw new IllegalStateException("nou set bean json convert.");
      }
      JAssert.checkNotNull(context, "context must not be null");
      JAssert.checkNotNull(param, "param must not be null");
      String encoding = JWebUtils.getResponseEncoding(context.getResponse());
      String browserName = JWebUtils.getBrowserName(context.getRequest());
      if (browserName.startsWith("ie")) {
         context.getResponse().setContentType("text/html; charset=" + encoding);
      } else {
         context.getResponse().setContentType("application/json; charset=" + encoding);
      }
   }

   /**
    * Bean - Json 渲染
    * 
    * @author jianggujin
    *
    */
   public static interface JBeanJsonRender {
      void render(Object bean, OutputStream out);
   }

   /**
    * 设置使用的转换器
    * 
    * @param beanJsonConverter
    */
   public static void useBeanJsonConverter(JBeanJsonRender beanJsonConverter) {
      JJsonRender.beanJsonConverter = beanJsonConverter;
   }

   public static synchronized void useJacksonBeanJsonConverter() {
      JJsonRender.beanJsonConverter = new com.jianggujin.modulelink.mvc.render.impl.JJacksonBeanJsonRender();
   }

   public static synchronized void useFastjsonBeanJsonConverter() {
      JJsonRender.beanJsonConverter = new com.jianggujin.modulelink.mvc.render.impl.JFastjsonBeanJsonRender();
   }

   private static void tryImplementation(Runnable runnable) {
      if (beanJsonConverter == null) {
         try {
            runnable.run();
         } catch (Throwable t) {
            // ignore
         }
      }
   }
}
