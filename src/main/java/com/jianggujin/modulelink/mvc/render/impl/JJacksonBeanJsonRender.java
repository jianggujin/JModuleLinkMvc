/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render.impl;

import java.io.OutputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jianggujin.modulelink.mvc.render.JJsonRender.JBeanJsonRender;
import com.jianggujin.modulelink.mvc.render.JRenderException;

/**
 * jackson实现
 * 
 * @author jianggujin
 *
 */
public class JJacksonBeanJsonRender implements JBeanJsonRender {
   private static ObjectMapper objectMapper = new ObjectMapper();

   @Override
   public void render(Object bean, OutputStream out) {
      try {
         objectMapper.writeValue(out, bean);
      } catch (Exception e) {
         throw new JRenderException(e);
      }
   }

   public static void setObjectMapper(ObjectMapper objectMapper) {
      JJacksonBeanJsonRender.objectMapper = objectMapper;
   }
}
