/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.render.JErrorRender.JErrorRenderParam;
import com.jianggujin.modulelink.mvc.render.JQrCodeRender.JQrCodeRenderParam;
import com.jianggujin.modulelink.mvc.render.JRedirectRender.JRedirectRenderParam;

/**
 * 渲染器管理器
 * 
 * @author jianggujin
 *
 */
public class JRenderManager {
   private final Map<String, JRender<?>> renderMap = new HashMap<String, JRender<?>>();
   private static JRenderManager instance = null;
   private JErrorRender errorRender = null;
   private JFileRender fileRender = null;
   private JJsonRender jsonRender = null;
   private JQrCodeRender qrCodeRender = null;
   private JRedirectRender redirectRender = null;
   private JRequestDispatcherRender requestDispatcherRender = null;
   private JTextRender textRender = null;
   private JXmlRender xmlRender = null;

   public static JRenderManager getInstance() {
      if (instance == null) {
         synchronized (JRenderManager.class) {
            if (instance == null) {
               instance = new JRenderManager();
            }
         }
      }
      return instance;
   }

   public <T> void regist(String renderName, JRender<T> render) {
      renderMap.put(renderName, render);
   }

   public JRender<?> getRender(String name) {
      return renderMap.get(name);
   }

   public JErrorRender getErrorRender() {
      if (errorRender == null) {
         synchronized (this) {
            if (errorRender == null) {
               errorRender = new JErrorRender();
            }
         }
      }
      return errorRender;
   }

   public JFileRender getFileRender() {
      if (fileRender == null) {
         synchronized (this) {
            if (fileRender == null) {
               fileRender = new JFileRender();
            }
         }
      }
      return fileRender;
   }

   public JJsonRender getJsonRender() {
      if (jsonRender == null) {
         synchronized (this) {
            if (jsonRender == null) {
               jsonRender = new JJsonRender();
            }
         }
      }
      return jsonRender;
   }

   public JQrCodeRender getQrCodeRender() {
      if (qrCodeRender == null) {
         synchronized (this) {
            if (qrCodeRender == null) {
               qrCodeRender = new JQrCodeRender();
            }
         }
      }
      return qrCodeRender;
   }

   public JRedirectRender getRedirectRender() {
      if (redirectRender == null) {
         synchronized (this) {
            if (redirectRender == null) {
               redirectRender = new JRedirectRender();
            }
         }
      }
      return redirectRender;
   }

   public JRequestDispatcherRender getRequestDispatcherRender() {
      if (requestDispatcherRender == null) {
         synchronized (this) {
            if (requestDispatcherRender == null) {
               requestDispatcherRender = new JRequestDispatcherRender();
            }
         }
      }
      return requestDispatcherRender;
   }

   public JTextRender getTextRender() {
      if (textRender == null) {
         synchronized (this) {
            if (textRender == null) {
               textRender = new JTextRender();
            }
         }
      }
      return textRender;
   }

   public JXmlRender getXmlRender() {
      if (xmlRender == null) {
         synchronized (this) {
            if (xmlRender == null) {
               xmlRender = new JXmlRender();
            }
         }
      }
      return xmlRender;
   }

   /**
    * 渲染异常
    * 
    */
   public static void renderError(JActionContext context, JErrorRenderParam param)
         throws IOException, ServletException {
      getInstance().getErrorRender().render(context, param);
   }

   /**
    * 渲染文件
    * 
    */
   public static void renderFile(JActionContext context, File param) throws IOException, ServletException {
      getInstance().getFileRender().render(context, param);
   }

   /**
    * 渲染json
    * 
    */
   public static void renderJson(JActionContext context, Object param) throws IOException, ServletException {
      getInstance().getJsonRender().render(context, param);
   }

   /**
    * 渲染二维码
    * 
    */
   public static void renderQrCode(JActionContext context, JQrCodeRenderParam param)
         throws IOException, ServletException {
      getInstance().getQrCodeRender().render(context, param);
   }

   /**
    * 渲染重定向
    * 
    */
   public static void renderRedirect(JActionContext context, JRedirectRenderParam param)
         throws IOException, ServletException {
      getInstance().getRedirectRender().render(context, param);
   }

   /**
    * 渲染请求转发
    * 
    * @param context
    * @param param
    * @throws IOException
    * @throws ServletException
    */
   public static void renderRequestDispatcherRender(JActionContext context, String param)
         throws IOException, ServletException {
      getInstance().getRequestDispatcherRender().render(context, param);
   }

   /**
    * 渲染文本
    * 
    */
   public static void renderText(JActionContext context, String param) throws IOException, ServletException {
      getInstance().getTextRender().render(context, param);
   }

   /**
    * 渲染XML
    * 
    */
   public static void renderXml(JActionContext context, Object param) throws IOException, ServletException {
      getInstance().getXmlRender().render(context, param);
   }
}
