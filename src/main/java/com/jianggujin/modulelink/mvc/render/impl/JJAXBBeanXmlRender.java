/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render.impl;

import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.jianggujin.modulelink.mvc.render.JRenderException;
import com.jianggujin.modulelink.mvc.render.JXmlRender.JBeanXmlRender;

/**
 * JAXB（Java Architecture for XML Binding)
 * 
 * @author jianggujin
 *
 */
public class JJAXBBeanXmlRender implements JBeanXmlRender {

   @Override
   public void render(Object bean, OutputStream out, String encoding) {
      try {
         JAXBContext context = JAXBContext.newInstance(bean.getClass());
         Marshaller marshaller = context.createMarshaller();
         marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
         marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);
         marshaller.marshal(bean, out);
      } catch (JAXBException e) {
         throw new JRenderException(e);
      }
   }
}
