/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render.impl;

import java.io.OutputStream;

import com.jianggujin.modulelink.mvc.render.JRenderException;
import com.jianggujin.modulelink.mvc.render.JXmlRender.JBeanXmlRender;
import com.thoughtworks.xstream.XStream;

/**
 * XStream实现
 * 
 * @author jianggujin
 *
 */
public class JXStreamBeanXmlRender implements JBeanXmlRender {

   @Override
   public void render(Object bean, OutputStream out, String encoding) {
      try {
         XStream xstream = new XStream();
         xstream.processAnnotations(bean.getClass());
         byte[] bytes = ("<?xml version=\"1.0\" encoding=\"" + encoding + "\" standalone=\"yes\"?>\r\n")
               .getBytes(encoding);
         out.write(bytes);
         xstream.toXML(bean, out);
      } catch (Exception e) {
         throw new JRenderException(e);
      }
   }

}
