/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.render.JCaptchaRender.JCaptchaRenderParam;
import com.jianggujin.modulelink.mvc.util.captcha.JCaptcha;
import com.jianggujin.modulelink.mvc.util.captcha.JGifCaptcha;
import com.jianggujin.modulelink.mvc.util.captcha.JSpecCaptcha;
import com.jianggujin.modulelink.util.JAssert;

/**
 * 验证码渲染
 * 
 * @author jianggujin
 *
 */
public class JCaptchaRender implements JRender<JCaptchaRenderParam> {

   @Override
   public void render(JActionContext context, JCaptchaRenderParam param) throws IOException, ServletException {
      JAssert.checkNotNull(context, "context must not be null");
      JAssert.checkNotNull(param, "param must not be null");
      int width = param.getWidth(), height = param.getHeight(), len = param.getLen();
      if (width < 0 || height < 0 || len < 0) {
         throw new IllegalArgumentException("width、height或len 不能小于 0");
      }
      HttpServletResponse response = context.getResponse();
      response.setHeader("Pragma", "No-cache");
      response.setHeader("Cache-Control", "no-cache");
      response.setDateHeader("Expires", 0);
      response.setContentType("image/png");

      try {

         JCaptcha captcha = null;
         switch (param.captchaType) {
         case GIF:
            response.setContentType("image/gif");
            captcha = new JGifCaptcha(width, height, len);
            break;
         case PNG:
            response.setContentType("image/png");
            captcha = new JSpecCaptcha(width, height, len);
            break;
         }
         if (param.sessionKey != null) {
            context.setSessionAttr(param.sessionKey, captcha.text().toLowerCase());
         }
         // 输出
         captcha.out(response.getOutputStream());
         // HttpSession session = request.getSession(true);
      } catch (IOException e) { // ClientAbortException、EofException 直接或间接继承自
                                // IOException
         String name = e.getClass().getSimpleName();
         if ("ClientAbortException".equals(name) || "EOFException".equals(name)) {
         } else {
            throw new JRenderException(e);
         }
      } catch (Exception e) {
         throw new JRenderException(e);
      }
   }

   public static class JCaptchaRenderParam {
      private JCaptchaType captchaType = JCaptchaType.PNG;
      private int width = 150, height = 40, len = 4;
      private String sessionKey;

      public JCaptchaType getCaptchaType() {
         return captchaType;
      }

      public void setCaptchaType(JCaptchaType captchaType) {
         this.captchaType = captchaType;
      }

      public int getWidth() {
         return width;
      }

      public void setWidth(int width) {
         this.width = width;
      }

      public int getHeight() {
         return height;
      }

      public void setHeight(int height) {
         this.height = height;
      }

      public int getLen() {
         return len;
      }

      public void setLen(int len) {
         this.len = len;
      }

      public String getSessionKey() {
         return sessionKey;
      }

      public void setSessionKey(String sessionKey) {
         this.sessionKey = sessionKey;
      }

   }

   public static enum JCaptchaType {
      PNG, GIF
   }
}
