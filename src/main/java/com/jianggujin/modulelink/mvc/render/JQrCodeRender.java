/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.jianggujin.modulelink.mvc.JActionContext;
import com.jianggujin.modulelink.mvc.render.JQrCodeRender.JQrCodeRenderParam;
import com.jianggujin.modulelink.util.JAssert;

/**
 * 二维码渲染
 * 
 * @author jianggujin
 *
 */
public class JQrCodeRender implements JRender<JQrCodeRenderParam> {

   @Override
   public void render(JActionContext context, JQrCodeRenderParam param) throws IOException, ServletException {
      JAssert.checkNotNull(context, "context must not be null");
      JAssert.checkNotNull(param, "param must not be null");
      int width = param.getWidth(), height = param.getHeight();
      if (width < 0 || height < 0) {
         throw new IllegalArgumentException("width 与 height 不能小于 0");
      }
      HttpServletResponse response = context.getResponse();
      response.setHeader("Pragma", "No-cache");
      response.setHeader("Cache-Control", "no-cache");
      response.setDateHeader("Expires", 0);
      response.setContentType("image/png");

      Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
      hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
      hints.put(EncodeHintType.MARGIN, 0); // 去掉白色边框，极度重要，否则二维码周围的白边会很宽
      if (param.getErrorCorrectionLevel() != null) {
         hints.put(EncodeHintType.ERROR_CORRECTION, param.getErrorCorrectionLevel());
      }

      try {
         // MultiFormatWriter 可支持多种格式的条形码，在此直接使用 QRCodeWriter，通过查看源码可知少创建一个对象
         // BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
         // BarcodeFormat.QR_CODE, width, height, hints);

         QRCodeWriter writer = new QRCodeWriter();
         BitMatrix bitMatrix = writer.encode(param.getContent(), BarcodeFormat.QR_CODE, width, height, hints);

         // 经测试 200 X 200 大小的二维码使用 "png" 格式只有 412B，而 "jpg" 却达到 15KB
         MatrixToImageWriter.writeToStream(bitMatrix, "png", response.getOutputStream()); // format:
                                                                                          // "jpg"、"png"
      } catch (IOException e) { // ClientAbortException、EofException 直接或间接继承自
                                // IOException
         String name = e.getClass().getSimpleName();
         if ("ClientAbortException".equals(name) || "EOFException".equals(name)) {
         } else {
            throw new JRenderException(e);
         }
      } catch (Exception e) {
         throw new JRenderException(e);
      }
   }

   public static class JQrCodeRenderParam {
      private final String content;
      private int width = 250;
      private int height = 250;
      /**
       * ErrorCorrectionLevel 是枚举类型，纠错能力从高到低共有四个级别：<br/>
       * H = ~30% correction<br/>
       * Q = ~25% correction<br/>
       * M = ~15% correction<br/>
       * L = ~7%
       */
      private ErrorCorrectionLevel errorCorrectionLevel;

      public JQrCodeRenderParam(String content) {
         JAssert.checkNotNull(content, "content must not be null.");
         this.content = content;
      }

      public String getContent() {
         return content;
      }

      public int getWidth() {
         return width;
      }

      public void setWidth(int width) {
         this.width = width;
      }

      public int getHeight() {
         return height;
      }

      public void setHeight(int height) {
         this.height = height;
      }

      public ErrorCorrectionLevel getErrorCorrectionLevel() {
         return errorCorrectionLevel;
      }

      public void setErrorCorrectionLevel(ErrorCorrectionLevel errorCorrectionLevel) {
         this.errorCorrectionLevel = errorCorrectionLevel;
      }
   }
}
