/**
 * Copyright 2018 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.modulelink.mvc.render.impl;

import java.io.OutputStream;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.jianggujin.modulelink.mvc.render.JRenderException;
import com.jianggujin.modulelink.mvc.render.JXmlRender.JBeanXmlRender;

/**
 * jackson实现
 * 
 * @author jianggujin
 *
 */
public class JJacksonBeanXmlRender implements JBeanXmlRender {
   private static XmlMapper xmlMapper = new XmlMapper();

   @Override
   public void render(Object bean, OutputStream out, String encoding) {
      try {
         byte[] bytes = ("<?xml version=\"1.0\" encoding=\"" + encoding + "\" standalone=\"yes\"?>\r\n")
               .getBytes(encoding);
         out.write(bytes);
         xmlMapper.writeValue(out, bean);
      } catch (Exception e) {
         throw new JRenderException(e);
      }
   }

   public static void setXmlMapper(XmlMapper xmlMapper) {
      JJacksonBeanXmlRender.xmlMapper = xmlMapper;
   }
}
