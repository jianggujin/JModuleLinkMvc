package com.jianggujin.modulelink.mvc.test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.jianggujin.modulelink.mvc.util.JInjector;

public class InjectorTest {
   @Test
   public void test() {
      Map<String, String> parasMap = new HashMap<String, String>();
      parasMap.put("name", "name");
      parasMap.put("time", "20180710101110");
      Bean bean = JInjector.injectBean(Bean.class, parasMap, false);
      System.out.println(bean);
   }

   public static class Bean {
      private String name;
      private Date time;

      public String getName() {
         return name;
      }

      public void setName(String name) {
         this.name = name;
      }

      public Date getTime() {
         return time;
      }

      public void setTime(Date time) {
         this.time = time;
      }

      @Override
      public String toString() {
         return "Bean [name=" + name + ", time=" + time + "]";
      }

   }
}
