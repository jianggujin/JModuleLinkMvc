package com.jianggujin.modulelink.mvc.test;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import com.jianggujin.modulelink.mvc.util.JDateUtils;

public class DateUtilsTest {
   @Test
   public void test() throws ParseException {
      Date date = JDateUtils.parseDateStrictly("20180710", new String[] { "yyyyMMdd" });
      System.out.println(date);
   }
}
